/**
 * 作者:  大柒
 * 功能:  Auto.js悬浮按钮样例(补间动画)
 * 日期:  2019/04/06
 * 备注:
 *    pro和免费版都可正常使用
 *
 * 待解决:
 *     (1)悬浮窗初始化的on事件. 没有找到这个方法目前用定时器解决此问题
 *     (2)补间动画结束的on事件. 试了很多都不可行目前用多线程解决此问题
 *     (3)快速双击主按钮会报错 (勉强修复了)
 */

//悬浮窗logo


importClass(java.lang.Runnable);
importClass(android.animation.ObjectAnimator)
importClass(android.animation.PropertyValuesHolder)
importClass(android.animation.ValueAnimator)
importClass(android.animation.AnimatorSet)
importClass(android.view.animation.AccelerateInterpolator)
importClass(android.view.animation.TranslateAnimation)
importClass(android.animation.ObjectAnimator)
importClass(android.animation.TimeInterpolator)
importClass(android.os.Bundle)
importClass(android.view.View)
importClass(android.view.Window)

importClass(android.view.animation.AccelerateDecelerateInterpolator)
importClass(android.view.animation.AccelerateInterpolator)
importClass(android.view.animation.AnticipateInterpolator)
importClass(android.view.animation.AnticipateOvershootInterpolator)
importClass(android.view.animation.BounceInterpolator)
importClass(android.view.animation.CycleInterpolator)
importClass(android.view.animation.DecelerateInterpolator)
importClass(android.view.animation.LinearInterpolator)
importClass(android.view.animation.OvershootInterpolator)
importClass(android.view.animation.PathInterpolator)
importClass(android.widget.Button)
importClass(android.widget.ImageView)
importClass(android.widget.TextView)

function dqFloaty() {


    var logo_switch = false;//全局: 悬浮窗的开启关闭检测
    var logo_buys = false;//全局: 开启和关闭时占用状态 防止多次点击触发
    var logo_fx = false;//全局: 悬浮按钮所在的方向 真左 假右
    var time_0, time_1, time_3;//全局: 定时器 点击退出悬浮窗时定时器关闭

    //可修改参数
    var logo_ms = 200//全局:  动画播放时间
    var DHK_ms = 200//全局:  对话框动画播放时间
    var tint_color = "#00000"//全局:  对话框图片颜色

    var dm = context.getResources().getDisplayMetrics();
    let wm = context.getSystemService(context.WINDOW_SERVICE);
    wm.getDefaultDisplay().getRealMetrics(dm);
    var screenOrien = true; // true为竖屏
    if (dm.widthPixels > dm.heightPixels) {
        screenOrien = false;
    }


    /**
     * 需要三个悬浮窗一起协作达到Auto.js悬浮窗效果
     * win  子菜单悬浮窗 处理子菜单选项点击事件
     * win_1  主悬浮按钮
     * win_2  悬浮按钮动画替身,只有在手指移动主按钮的时候才会被触发
     * 触发时,替身Y值会跟主按钮Y值绑定一起,手指弹起时代替主按钮显示跳动的小球动画
     */
    var win = floaty.rawWindow(
        <frame >//子菜单悬浮窗
            <frame id="id_logo" w="150" h="210" alpha="0"  >
                <frame id="id_0" w="44" h="44" margin="33 0 0 0" alpha="1">
                    <img w="44" h="44" src="#009687" circle="true" />
                    <img w="28" h="28" src="@drawable/ic_perm_identity_black_48dp" tint="#ffffff" gravity="center" layout_gravity="center" />
                    <img id="id_0_click" w="*" h="*" src="#ffffff" circle="true" alpha="0" />
                </frame>
                <frame id="id_1" w="44" h="44" margin="86 28 0 0" alpha="1">
                    <img w="44" h="44" src="#ee534f" circle="true" />
                    <img w="28" h="28" src="@drawable/ic_assignment_black_48dp" tint="#ffffff" gravity="center" layout_gravity="center" />
                    <img id="id_1_click" w="*" h="*" src="#ffffff" circle="true" alpha="0" />
                </frame>
                <frame id="id_2" w="44" h="44" margin="0 83 0 0" alpha="1" gravity="right" layout_gravity="right">
                    <img w="44" h="44" src="#40a5f3" circle="true" />
                    <img w="28" h="28" src="@drawable/ic_play_arrow_black_48dp" tint="#ffffff" margin="8" />
                    <img id="id_2_click" w="*" h="*" src="#ffffff" circle="true" alpha="0" />
                </frame>
                <frame id="id_3" w="44" h="44" margin="86 0 0 28" alpha="1" gravity="bottom" layout_gravity="bottom">
                    <img w="44" h="44" src="#fbd834" circle="true" />
                    <img w="28" h="28" src="@drawable/ic_clear_black_48dp" tint="#ffffff" margin="8" />
                    <img id="id_3_click" w="*" h="*" src="#ffffff" circle="true" alpha="0" />
                </frame>
                <frame id="id_4" w="44" h="44" margin="33 0 0 0" alpha="1" gravity="bottom" layout_gravity="bottom">
                    <img w="44" h="44" src="#bfc1c0" circle="true" />
                    <img w="28" h="28" src="@drawable/ic_settings_black_48dp" tint="#ffffff" margin="8" />
                    <img id="id_4_click" w="*" h="*" src="#ffffff" circle="true" alpha="0" />
                </frame>
            </frame>
            <frame id="logo" w="44" h="44" marginTop="83" alpha="1" />
            <frame id="logo_1" w="44" h="44" margin="0 83 22 0" alpha="1" layout_gravity="right" />
        </frame>
    )
    win.setTouchable(false);//设置子菜单不接收触摸消息

    var win_1 = floaty.rawWindow(
        <frame id="logo" w="44" h="44" alpha="0.4" >//悬浮按钮
            <img w="44" h="44" src="#ffffff" circle="true" alpha="0.8" />
            <img id="img_logo" w="32" h="32" src="https://pro.autojs.org/images/logo.png" gravity="center" layout_gravity="center" />
            <img id="logo_click" w="*" h="*" src="#ffffff" alpha="0" />
        </frame>
    )
    win_1.setPosition(dm.widthPixels - 105, dm.heightPixels / 2) //悬浮按钮定位

    var win_2 = floaty.rawWindow(
        <frame id="logo" h="44" alpha="0" >//悬浮按钮 弹性替身
            <img w="44" h="44" src="#ffffff" circle="true" alpha="0.8" />
            <img id="img_logo" w="32" h="32" src="https://pro.autojs.org/images/logo.png" margin="6 6" />
        </frame>
    )
    win_2.logo.getLayoutParams().width = dm.widthPixels;
    win_2.setTouchable(false);//设置弹性替身不接收触摸消息

    /**
     * 脚本广播事件
     */
    var XY = [], XY1 = [], TT = [], TT1 = [], img_dp = {}, dpZ = 0, logo_right = 0, dpB = 0, dp_H = 0
    events.broadcast.on("定时器关闭", function (X) { clearInterval(X) })
    events.broadcast.on("悬浮开关", function (X) {
        logo_buys = true
        ui.run(function () {
            switch (X) {
                case true:
                    win.id_logo.setVisibility(0)
                    win.setTouchable(true);
                    logo_switch = true
                    break;
                case false:
                    win.id_logo.setVisibility(4)
                    win.setTouchable(false);
                    logo_switch = false
            }
            logo_buys = false;
        })

    });

    events.broadcast.on("悬浮显示", function (X1) {
        logo_buys = true;
        ui.run(function () {
            win_2.logo.attr("alpha", "0");
            win_1.logo.attr("alpha", "0.4");
            logo_buys = false;
        })
    });

    /**
     * 等待悬浮窗初始化
     */
    var terid = setInterval(() => {
        if (TT.length == 0 && win.logo.getY() > 0) {// 不知道界面初始化的事件  只能放到这里将就下了
            ui.run(function () {
                TT = [win.logo.getX(), win.logo.getY()], TT1 = [win.logo_1.getLeft(), win.logo_1.getTop()], anX = [], anY = []// 获取logo 绝对坐标
                XY = [
                    [win.id_0, TT[0] - win.id_0.getX(), TT[1] - win.id_0.getY()],//  获取子菜单 视图和子菜单与logo绝对坐标差值
                    [win.id_1, TT[0] - win.id_1.getX(), TT[1] - win.id_1.getY()],
                    [win.id_2, TT[0] - win.id_2.getX(), 0],
                    [win.id_3, TT[0] - win.id_3.getX(), TT[1] - win.id_3.getY()],
                    [win.id_4, TT[0] - win.id_4.getX(), TT[1] - win.id_4.getY()]]
                dpZ = XY[0][2] / 83
                dpB = dpZ * 22
                XY1 = [
                    [parseInt(dpZ * 41), TT1[0] - win.id_0.getLeft(), TT1[1] - win.id_0.getTop()],
                    [parseInt(dpZ * -65), TT1[0] - win.id_1.getLeft(), TT1[1] - win.id_1.getTop()],
                    [parseInt(dpZ * -106), TT1[0] - win.id_2.getLeft(), TT1[1] - win.id_2.getTop()],
                    [parseInt(dpZ * -65), TT1[0] - win.id_3.getLeft(), TT1[1] - win.id_3.getTop()],
                    [parseInt(dpZ * 41), TT1[0] - win.id_4.getLeft(), TT1[1] - win.id_4.getTop()]]
                img_dp.h_b = XY[0][2]//两个悬浮窗Y差值
                img_dp.w = parseInt(dpZ * 9)//计算logo左边隐藏时 X值
                img_dp.ww = parseInt(dpZ * (44 - 9))//计算logo右边隐藏时 X值
                logo_right = win.id_2.getX() - parseInt(dpZ * 22)
                win_1.setPosition(dm.widthPixels - img_dp.ww, dm.heightPixels / 2) //悬浮按钮定位
                win.id_logo.setVisibility(4)
                win.id_logo.attr("alpha", "1")
                events.broadcast.emit("定时器关闭", terid)
            })
        }
    }, 100)

    time_0 = setInterval(() => {
        // 处理屏幕旋转
        var screenOrien2 = true;
        if (dm.widthPixels > dm.heightPixels) {
            screenOrien2 = false;
        }
        if (screenOrien2 != screenOrien) { // 屏幕发生旋转了
            screenOrien = screenOrien2;
            wm.getDefaultDisplay().getRealMetrics(dm); // 重新更新一下dm的真实宽高
            ui.run(function () {
                logo_fx = false;
                win_1.setPosition(dm.widthPixels - img_dp.ww, dm.heightPixels / 2) //悬浮按钮定位
                win_2.logo.getLayoutParams().width = dm.widthPixels;
            });
        }
    }, 1000)

    /**
     * 子菜单点击事件
     */
    function img_down() {
        win_1.logo.attr("alpha", "0.4")
        logo_switch = false;
        动画();
    }
    win.id_0_click.on("click", () => {
        toastLog("个人中心");
        img_down();
    })

    win.id_1_click.on("click", () => {
        img_down();
        events.broadcast.emit('DQFLOATY_LOG_CLICK', '');
    })

    // 启动脚本
    var scriptThread = null;
    win.id_2_click.on("click", () => {
        img_down();
        if (null != scriptThread) {
            scriptThread.interrupt();
        }
        var script = require('../script');
        scriptThread = threads.start(function () {
            toastLog('启动脚本');
            try {
                var scriptNew = new script();
                scriptNew.setUserConfigs(); // 不传参数，默认从storage里面读配置
                scriptNew.run();
            } catch (e) {
                console.log(e);
                ui.run(function () {
                    win_1.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
                    win_2.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
                });
            }
        });
        win_1.img_logo.setColorFilter(colors.argb(255, 255, 153, 0));
        win_2.img_logo.setColorFilter(colors.argb(255, 255, 153, 0));
    })

    // 停止脚本
    win.id_3_click.on("click", () => {
        img_down();
        if (null != scriptThread) {
            scriptThread.interrupt();
        }
        win_1.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
        win_2.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
        toastLog("结束脚本");
    });

    // 点击设置
    events.broadcast.on('DQFLOATY_STOP_CLICK', function () {
        img_down()
        if (null != scriptThread) {
            scriptThread.interrupt();
        }
        win_1.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
        win_2.img_logo.setColorFilter(colors.argb(0, 255, 153, 0));
    });

    win.id_4_click.on("click", () => {
        events.broadcast.emit('DQFLOATY_SETTING_CLICK', '');
        img_down();
    });


    /**
     * 补间动画
     */
    function 动画() {
        var anX = [], anY = [], slX = [], slY = []
        if (logo_switch) {
            if (logo_fx) {
                for (let i = 0; i < XY.length; i++) {
                    anX[i] = ObjectAnimator.ofFloat(XY[i][0], "translationX", parseInt(XY[i][1]), 0);
                    anY[i] = ObjectAnimator.ofFloat(XY[i][0], "translationY", parseInt(XY[i][2]), 0);
                    slX[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleX", 0, 1)
                    slY[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleY", 0, 1)
                }
            } else {
                for (let i = 0; i < XY.length; i++) {
                    anX[i] = ObjectAnimator.ofFloat(XY[i][0], "translationX", XY1[i][1], XY1[i][0]);
                    anY[i] = ObjectAnimator.ofFloat(XY[i][0], "translationY", XY1[i][2], 0);
                    slX[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleX", 0, 1)
                    slY[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleY", 0, 1)
                }
            }
        } else {
            if (logo_fx) {
                for (let i = 0; i < XY.length; i++) {
                    anX[i] = ObjectAnimator.ofFloat(XY[i][0], "translationX", 0, parseInt(XY[i][1]));
                    anY[i] = ObjectAnimator.ofFloat(XY[i][0], "translationY", 0, parseInt(XY[i][2]));
                    slX[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleX", 1, 0)
                    slY[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleY", 1, 0)
                }
            } else {
                for (let i = 0; i < XY.length; i++) {
                    anX[i] = ObjectAnimator.ofFloat(XY[i][0], "translationX", XY1[i][0], XY1[i][1]);
                    anY[i] = ObjectAnimator.ofFloat(XY[i][0], "translationY", 0, XY1[i][2]);
                    slX[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleX", 1, 0)
                    slY[i] = ObjectAnimator.ofFloat(XY[i][0], "scaleY", 1, 0)
                }
            }
        }
        set = new AnimatorSet();
        set.playTogether(
            anX[0], anX[1], anX[2], anX[3], anX[4],
            anY[0], anY[1], anY[2], anY[3], anY[4],
            slX[0], slX[1], slX[2], slX[3], slX[4],
            slY[0], slY[1], slY[2], slY[3], slY[4]);
        set.setDuration(logo_ms);
        threads.start(function () {//动画的结束事件一直没有明白 只能拿线程代替了
            if (logo_switch) {
                events.broadcast.emit("悬浮开关", true)
                sleep(logo_ms)
            } else {
                sleep(logo_ms + 100)
                events.broadcast.emit("悬浮开关", false)
            }
        });
        set.start();
    }
    function 对话框动画(X, Y, Z) {//X布尔值 标识显示还是隐藏 Y背景的视图 Z对话框的视图
        var anX = [], anY = [], slX = [], slY = []
        if (X) {
            anX = ObjectAnimator.ofFloat(Z, "translationX", win_1.getX() - (Z.getRight() / 2) + dpB - Z.getLeft(), 0);
            anY = ObjectAnimator.ofFloat(Z, "translationY", win_1.getY() - (Z.getBottom() / 2) + img_dp.h_b - Z.getTop(), 0);
            slX = ObjectAnimator.ofFloat(Z, "scaleX", 0, 1)
            slY = ObjectAnimator.ofFloat(Z, "scaleY", 0, 1)
            animator = ObjectAnimator.ofFloat(Y, "alpha", 0, 0.5)
            animator1 = ObjectAnimator.ofFloat(Z, "alpha", 1, 1)
        } else {
            anX = ObjectAnimator.ofFloat(Z, "translationX", 0, win_1.getX() - (Z.getRight() / 2) + dpB - Z.getLeft());
            anY = ObjectAnimator.ofFloat(Z, "translationY", 0, win_1.getY() - (Z.getBottom() / 2) + img_dp.h_b - Z.getTop());
            slX = ObjectAnimator.ofFloat(Z, "scaleX", 1, 0)
            slY = ObjectAnimator.ofFloat(Z, "scaleY", 1, 0)
            animator = ObjectAnimator.ofFloat(Y, "alpha", 0.5, 0)
            animator1 = ObjectAnimator.ofFloat(Z, "alpha", 1, 0)
        }
        set = new AnimatorSet()
        set.playTogether(
            anX, anY, slX, slY, animator, animator1);
        set.setDuration(DHK_ms);
        set.start();
    }

    //记录按键被按下时的触摸坐标
    var x = 0,
        y = 0;
    //记录按键被按下时的悬浮窗位置
    var windowX, windowY; G_Y = 0
    //记录按键被按下的时间以便判断长按等动作
    var downTime; yd = false;
    win_1.logo.setOnTouchListener(function (view, event) {
        if (logo_buys) { return }
        switch (event.getAction()) {
            case event.ACTION_DOWN:
                x = event.getRawX();
                y = event.getRawY();
                windowX = win_1.getX();
                windowY = win_1.getY();
                downTime = new Date().getTime();
                return true;
            case event.ACTION_MOVE:
                if (logo_switch) { return true; }
                if (!yd) {//如果移动的距离大于h值 则判断为移动 yd为真
                    if (Math.abs(event.getRawY() - y) > 30 || Math.abs(event.getRawX() - x) > 30) { win_1.logo.attr("alpha", "1"); yd = true }
                } else {//移动手指时调整两个悬浮窗位置
                    win_1.setPosition(windowX + (event.getRawX() - x),//悬浮按钮定位
                        windowY + (event.getRawY() - y));
                    win_2.setPosition(0, windowY + (event.getRawY() - y));//弹性 替身定位(隐藏看不到的,松开手指才会出现)
                }
                return true;
            case event.ACTION_UP:                //手指弹起
                //触摸时间小于 200毫秒 并且移动距离小于30 则判断为 点击
                if (logo_buys) { return }//如果在动画正在播放中则退出事件 无操作
                if (Math.abs(event.getRawY() - y) < 30 && Math.abs(event.getRawX() - x) < 30) {
                    if (logo_switch) {
                        logo_switch = false;
                        win_1.logo.attr("alpha", "0.4");
                    } else if (logo_fx) {
                        win.setPosition(windowX + (event.getRawX() - x),
                            windowY + (event.getRawY() - y) - img_dp.h_b);
                        win.id_logo.setVisibility(0);
                        logo_switch = true
                        win_1.logo.attr("alpha", "0.9");
                    } else {
                        win.setPosition(win_1.getX() + (event.getRawX() - x) - logo_right,
                            win_1.getY() + (event.getRawY() - y) - img_dp.h_b);
                        win.id_logo.setVisibility(0);
                        logo_switch = true;
                        win_1.logo.attr("alpha", "0.9");
                    }
                    动画();
                } else if (!logo_switch) {
                    G_Y = windowY + (event.getRawY() - y)
                    win_1.logo.attr("alpha", "0.4")

                    if (windowX + (event.getRawX() - x) < dm.widthPixels / 2) {
                        logo_fx = true
                        animator = ObjectAnimator.ofFloat(win_2.logo, "translationX", windowX + (event.getRawX() - x), 0 - img_dp.w);
                        mTimeInterpolator = new BounceInterpolator();
                        animator.setInterpolator(mTimeInterpolator);
                        animator.setDuration(300);
                        win_2.logo.attr("alpha", "0.4"); //动画 替身上场
                        win_1.logo.attr("alpha", "0"); //悬浮按钮隐藏
                        win_1.setPosition(0 - img_dp.w, G_Y); //悬浮按钮移动到终点位置等待替身动画结束
                        animator.start();
                    } else {
                        logo_fx = false
                        animator = ObjectAnimator.ofFloat(win_2.logo, "translationX", windowX + (event.getRawX() - x), dm.widthPixels - img_dp.ww);
                        mTimeInterpolator = new BounceInterpolator();
                        animator.setInterpolator(mTimeInterpolator);
                        animator.setDuration(300);
                        win_2.logo.attr("alpha", "0.4")//动画替身上场
                        win_1.logo.attr("alpha", "0");//悬浮按钮隐藏
                        win_1.setPosition(dm.widthPixels - img_dp.ww, G_Y)//悬浮按钮移动到终点位置等待替身动画结束
                        animator.start();
                    }
                    threads.start(function () {//动画的结束事件一直没有明白 只能拿线程代替了
                        sleep(logo_ms + 100)
                        events.broadcast.emit("悬浮显示", 0)
                    });
                }
                yd = false
                return true;
        }
        return true;
    });

}
module.exports = {
    render: dqFloaty
};