var multiColor = {};
var multiColorNames = [];
for (var i = 0; i < multiColorNames.length; i++) {
    multiColor[multiColorNames[i]] = require('../multi_colors/21601080/' + multiColorNames[i]);
}

module.exports = [
    null, // 0 放着占位,后续可能会放必执行的东西
    {
        id: 1,
        name: '战前布阵_确定',
        data: [
            {
                judgePoints: [
                    { x:   33, y:   32, c: 0x313131, i: true },
                    { x:   66, y:   59, c: 0xffffff, i: true },
                    { x:  204, y:   54, c: 0xfba159, i: true },
                    { x: 1908, y:  907, c: 0xff9d51, i: true },
                    { x: 1893, y:  976, c: 0x702c07, i: true },
                    { x: 1936, y:  976, c: 0x702c07, i: true },
                    { x: 1904, y:  832, c: 0xffffff, i: true },
                ],
                operaPoints: [{ x: 1829, y: 861, ox: 175, oy: 170, ad: 5000 }]
            }
        ],
    },
    {
        id: 2,
        name: '退出结算_继续',
        data: [
            { // 战斗胜利
                judgePoints: [
                    { x: 1836, y:  984, c: 0xffffff, i: true },
                    { x: 1836, y:  989, c: 0xffffff, i: true },
                    { x: 1836, y:  997, c: 0xffffff, i: true },
                    { x: 1982, y:  995, c: 0xffffff, i: true },
                    { x: 1983, y: 1028, c: 0x1a1a1a, i: true },
                    { x: 2014, y:  999, c: 0xffffff, i: true },
                    { x: 2033, y: 1022, c: 0x1a1a1a, i: true },
                    { x: 2060, y:  999, c: 0xfa8444, i: true },
                ],
                operaPoints: [{ x: 50, y: 50, ox: 2060, oy: 980, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:  256, y:  140, c: 0xfa8444, i: true },
                    { x:  220, y:  142, c: 0xfefefe, i: true },
                    { x:  313, y:  140, c: 0x000000, i: true },
                    { x:  280, y:  126, c: 0x000000, i: true },
                    { x:  388, y:  117, c: 0xc2c4c2, i: true },
                    { x:  510, y:  131, c: 0x000000, i: true },
                    { x: 1277, y:  234, c: 0xfa8444, i: true },
                    { x: 1606, y:  843, c: 0xffa355, i: true },
                    // { x: 1383, y:  846, c: 0x313131, i: true },
                ],
                operaPoints: [{ x: 1597, y: 835, ox: 271, oy: 86, ad: 2000 }]
            }
        ]
    },
    {
        id: 3,
        name: '退出结算_再次挑战',
        data: [
            { // 战斗胜利
                judgePoints: [
                    { x: 1836, y:  984, c: 0xffffff, i: true },
                    { x: 1836, y:  989, c: 0xffffff, i: true },
                    { x: 1836, y:  997, c: 0xffffff, i: true },
                    { x: 1982, y:  995, c: 0xffffff, i: true },
                    { x: 1983, y: 1028, c: 0x1a1a1a, i: true },
                    { x: 2014, y:  999, c: 0xffffff, i: true },
                    { x: 2033, y: 1022, c: 0x1a1a1a, i: true },
                    { x: 2060, y:  999, c: 0xfa8444, i: true },
                ],
                operaPoints: [{ x: 50, y: 50, ox: 2060, oy: 980, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:  256, y:  140, c: 0xfa8444, i: true },
                    { x:  220, y:  142, c: 0xfefefe, i: true },
                    { x:  313, y:  140, c: 0x000000, i: true },
                    { x:  280, y:  126, c: 0x000000, i: true },
                    { x:  388, y:  117, c: 0xc2c4c2, i: true },
                    { x:  510, y:  131, c: 0x000000, i: true },
                    { x: 1277, y:  234, c: 0xfa8444, i: true },
                    { x: 1606, y:  843, c: 0xffa355, i: true },
                    { x: 1383, y:  846, c: 0x313131, i: true },
                ],
                operaPoints: [{ x: 1268, y: 835, ox: 271, oy: 80, ad: 2000 }]
            }
        ]
    },
    {
        id: 4,
        name: '限时副本发现',
        data: [{
            judgePoints: [
                { x:  889, y:  344, c: 0xffffff, i: true },
                { x:  989, y:  338, c: 0xffffff, i: true },
                { x:  402, y:  429, c: 0xffffff, i: true },
                { x:  380, y:  503, c: 0xef7f42, i: true },
                { x:  436, y:  474, c: 0x515151, i: true },
                { x:  762, y:  463, c: 0xfcaf85, i: true },
                { x:  764, y:  678, c: 0xffffff, i: true },
            ],
            operaPoints: [{ x: 1008, y: 603, ox: 392, oy: 124, ad: 2000 }]
        }]
    },
    {
        id: 5,
        name: '组队大厅_邀请好友',
        data: [{
            secondJudgeDelay: 5000,
            judgePoints: [
                { x:   38, y:   38, c: 0x313131, i: true },
                { x:   67, y:   57, c: 0xffffff, i: true },
                { x:  182, y:   36, c: 0xfba059, i: true },
                { x:  195, y:   41, c: 0x7e3e16, i: true },
                { x:  200, y:   51, c: 0xfba159, i: true },
                { x:  305, y:   70, c: 0xffffff, i: true },
                { x:  338, y:   57, c: 0xffffff, i: true },
                { x:  438, y:   53, c: 0xffffff, i: true },
                { x:  480, y:   47, c: 0xffffff, i: true },
                { x:  602, y:  980, c: 0x343434, i: true },
                { x:  559, y:  998, c: 0xffffff, i: true },
                { x: 1150, y:  967, c: 0xce7141, i: true },
                { x: 1188, y:  986, c: 0xfda958, i: true },
                { x: 1265, y: 1026, c: 0xf4efec, i: true },
                { x: 2109, y:   43, c: 0x373737, i: true },
                // { x:  830, y:  540, c: 0xfa8444, i: true },
                { x: 1741, y:  540, c: 0xfa8444, i: true },
            ],
            operaPoints: [
                { x: 1721, y: 508, ox: 61, oy: 62, ad: 1000 },
                { x: 831, y: 288, ox: 168, oy: 67, ad: 500 },
                { x: 1362, y: 399, ox: 153, oy: 81, ad: 500 },
                { x: 1367, y: 559, ox: 141, oy: 65, ad: 500 },
                // { x: 115, y: 210, ox: 256, oy: 647, ad: [10000, 20000] },
                { x: 115, y: 210, ox: 256, oy: 647, ad: 500 },
            ]
        }],
    },
    {
        id: 6,
        name: '组队大厅_三人开始',
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   38, y:   38, c: 0x313131, i: true },
                { x:   67, y:   57, c: 0xffffff, i: true },
                { x:  182, y:   36, c: 0xfba059, i: true },
                { x:  195, y:   41, c: 0x7e3e16, i: true },
                { x:  200, y:   51, c: 0xfba159, i: true },
                { x:  305, y:   70, c: 0xffffff, i: true },
                { x:  338, y:   57, c: 0xffffff, i: true },
                { x:  438, y:   53, c: 0xffffff, i: true },
                { x:  480, y:   47, c: 0xffffff, i: true },
                { x:  602, y:  980, c: 0x343434, i: true },
                { x:  559, y:  998, c: 0xffffff, i: true },
                { x: 1150, y:  967, c: 0xce7141, i: true },
                { x: 1188, y:  986, c: 0xfda958, i: true },
                { x: 1265, y: 1026, c: 0xf4efec, i: true },
                { x: 2109, y:   43, c: 0x373737, i: true },
                { x:  830, y:  540, c: 0xfa8444, i: false },
                { x: 1741, y:  540, c: 0xfa8444, i: false },
            ],
            operaPoints: [
                { x: 1153, y: 966, ox: 270, oy: 77, ad: [1000, 3000] },
            ]
        }],
    },
    {
        id: 7,
        name: '组队大厅_开始',
        data: function () {
            let clickConfig = {
                name: '组队大厅_开始',
                data: [{
                    secondJudgeDelay: 2000,
                    judgePoints: [
                        { x:   38, y:   38, c: 0x313131, i: true },
                        { x:   67, y:   57, c: 0xffffff, i: true },
                        { x:  182, y:   36, c: 0xfba059, i: true },
                        { x:  195, y:   41, c: 0x7e3e16, i: true },
                        { x:  200, y:   51, c: 0xfba159, i: true },
                        { x:  305, y:   70, c: 0xffffff, i: true },
                        { x:  338, y:   57, c: 0xffffff, i: true },
                        { x:  438, y:   53, c: 0xffffff, i: true },
                        { x:  480, y:   47, c: 0xffffff, i: true },
                        { x:  602, y:  980, c: 0x343434, i: true },
                        { x:  559, y:  998, c: 0xffffff, i: true },
                        { x: 1150, y:  967, c: 0xce7141, i: true },
                        { x: 1188, y:  986, c: 0xfda958, i: true },
                        { x: 1265, y: 1026, c: 0xf4efec, i: true },
                        { x: 2109, y:   43, c: 0x373737, i: true },
                        { x:  830, y:  540, c: 0xfa8444, i: false },
                        // { x: 1741, y:  540, c: 0xfa8444, i: false },
                    ],
                    operaPoints: [
                        { x: 1153, y: 966, ox: 270, oy: 77, ad: [1000, 3000] },
                    ]
                }],
            }
            let successTimes = 0;
            let success = false;
            while (this.commonClick(clickConfig)) {
                success = true;
                if (++successTimes >= this.userConfigs.zddt_ks_times_to_leave) {
                    this.automator.press(random(21, 129), random(22, 89), random(10, 100));
                    toastLog('发现卡车行为，退出房间。');
                    break;
                };
                this.captureScreen();
            }
            return success;
        }
    },
    {
        id: 8,
        name: '战斗中_退出战斗',
        data: function () {
            return this.commonClick({
                id: 8,
                name: '战斗中_退出战斗',
                data: [{ // 究极组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:  694, y:   47, c: 0xa03637, i: true },
                        { x:  772, y:   49, c: 0x3492ff, i: true },
                        // { x:  156, y:   76, c: 0xe45f18, i: true },
                        { x:  156, y:   97, c: 0x1e87e8, i: true },
                        { x:   37, y:   59, c: 0xfdfdfd, i: true },
                    ],
                    operaPoints: [
                        { x: 2094, y: 34, ox: 37, oy: 37, ad: 1000 },
                        { x: 617,  y: 886, ox: 251, oy: 114, ad: 1000 },
                        { x: 1136, y: 678, ox: 342, oy: 113, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{ // 章鱼组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:   32, y:   82, c: 0xffffff, i: true },
                        { x:  137, y:   84, c: 0xffffff, i: true },
                        { x:  151, y:   96, c: 0x1e87e8, i: true },
                        { x:  654, y:  170, c: 0xff7835, i: true },
                        { x: 1079, y:   43, c: 0xffffff, i: true },
                        { x: 1079, y:   55, c: 0xffffff, i: true },
                    ],
                    operaPoints: [
                        { x: 2094, y: 34, ox: 37, oy: 37, ad: 1000 },
                        { x: 617,  y: 886, ox: 251, oy: 114, ad: 1000 },
                        { x: 1136, y: 678, ox: 342, oy: 113, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  896, y:  211, c: 0xffffff, i: true },
                        { x:  859, y:  326, c: 0xffffff, i: true },
                        { x:  339, y:  482, c: 0xffffff, i: true },
                        { x:  820, y:  495, c: 0xffffff, i: true },
                        { x:  685, y:  910, c: 0x303030, i: true },
                        { x: 1260, y:  909, c: 0xffa053, i: true },
                        { x:  724, y:  933, c: 0xd9d9d9, i: true },
                        { x: 1407, y:  937, c: 0x702c07, i: true },
                    ],
                    operaPoints: [
                        { x: 617,  y: 886, ox: 251, oy: 114, ad: 1000 },
                        { x: 1136, y: 678, ox: 342, oy: 113, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  713, y:  386, c: 0xffffff, i: true },
                        { x:  953, y:  369, c: 0xffffff, i: true },
                        { x:  812, y:  441, c: 0x000000, i: true },
                        { x:  871, y:  436, c: 0xffffff, i: true },
                        { x:  877, y:  436, c: 0x000000, i: true },
                        { x:  704, y:  699, c: 0x313131, i: true },
                        { x: 1153, y:  695, c: 0xffa355, i: true },
                        { x:  828, y:  730, c: 0xffffff, i: true },
                        { x: 1289, y:  734, c: 0x702c07, i: true },
                    ],
                    operaPoints: [
                        { x: 1136, y: 678, ox: 342, oy: 113, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }, { // 战斗失败
                    judgePoints: [
                        { x:  748, y:  665, c: 0xfa8444, i: true },
                        { x:  785, y:  642, c: 0xffffff, i: true },
                        { x:  794, y:  663, c: 0x000000, i: true },
                        { x:  767, y:  457, c: 0xe8e8e8, i: true },
                        { x: 1074, y:  539, c: 0xff921e, i: true },
                        { x: 1175, y:  538, c: 0xfa8444, i: true },
                    ],
                    operaPoints: [
                        { x: 50, y: 50, ox: 2060, oy: 980, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }]
            });
        },
    }, {
        id: 9,
        name: '接受邀请',
        data: [{
            judgePoints: [
                { x: 1944, y:  165, c: 0x000000, i: true },
                { x: 1950, y:  177, c: 0x000000, i: true },
                // { x: 1560, y:  232, c: 0xffffff, i: true },
                // { x: 1560, y:  255, c: 0xffffff, i: true },
                { x: 1705, y:  217, c: 0xffffff, i: true },
                { x: 1729, y:  245, c: 0xffffff, i: true },
                { x: 1735, y:  267, c: 0x000000, i: true },
                { x: 1838, y:  261, c: 0xffffff, i: true },
                { x: 1832, y:  280, c: 0x000000, i: true },
                { x: 1336, y:  342, c: 0xfa8444, i: true },
                { x: 2003, y:  152, c: 0xff0000, i: true },
                { x: 1998, y:  152, c: 0xff0000, i: true },
                { x: 2009, y:  152, c: 0xffffff, i: true },
            ],
            operaPoints: [
                { x: 1820, y: 248, ox: 40, oy: 37, ad: 500 },
            ]
        }, {
            judgePoints: [
                { x: 2000, y:  150, c: 0xff0000, i: true },
                { x: 1998, y:  142, c: 0xff0000, i: true },
                { x: 2009, y:  153, c: 0xffffff, i: true },
                { x: 2020, y:  151, c: 0xff0000, i: true },
                { x: 1945, y:  178, c: 0x000000, i: true },
                { x: 1966, y:  202, c: 0x000000, i: true },
                { x: 1991, y:  191, c: 0x55f1ff, i: true },
                { x: 2002, y:  191, c: 0x55f1ff, i: true },
            ],
            operaPoints: [
                { x: 1948, y: 156, ox: 57, oy: 59, ad: 500 },
            ]
        }],
    }, {
        id: 10,
        name: '组队大厅_准备',
        data: [{
            judgePoints: [
                { x:   37, y:   31, c: 0x313131, i: true },
                { x:   40, y:   49, c: 0x313131, i: true },
                { x:  180, y:   32, c: 0xfba159, i: true },
                { x:  179, y:   41, c: 0xfa9e57, i: true },
                { x:   29, y:  171, c: 0xfa8444, i: true },
                { x:   28, y:  178, c: 0xfa8444, i: true },
                { x:  476, y:  976, c: 0x323232, i: true },
                { x:  476, y:  981, c: 0x323232, i: true },
                { x:  559, y: 1005, c: 0xffffff, i: true },
                { x: 1228, y: 1006, c: 0xf5954f, i: true },
            ],
            operaPoints: [
                { x: 1155, y: 969, ox: 262, oy: 74, ad: [1000, 3000] },
            ]
        }],
    }, {
        id: 11,
        name: '究极_创建组队',
        data: [{
            judgePoints: [
                { x:   38, y:   34, c: 0x313131, i: true },
                { x:   65, y:   57, c: 0xffffff, i: true },
                { x:  177, y:   29, c: 0xfba059, i: true },
                { x: 1279, y:  955, c: 0xf7a658, i: true },
                { x: 1365, y:  981, c: 0x702c07, i: true },
                { x: 1309, y:  984, c: 0xfa9d52, i: true },
                { x: 1553, y:  965, c: 0xfead5a, i: true },
                { x: 2090, y:   55, c: 0xffffff, i: true },
                { x:  300, y:   45, c: 0xffffff, i: true },
                { x:  276, y:   56, c: 0x162427, i: true },
            ],
            operaPoints: [
                { x: 1255, y: 939, ox: 317, oy: 109, ad: 200},
                { x: 932, y: 766, ox: 330, oy: 79, ad: 2000},
            ]
        }, {
            judgePoints: [
                { x:  697, y:  213, c: 0xfa8444, i: true },
                { x:  726, y:  208, c: 0xffffff, i: true },
                { x:  874, y:  215, c: 0x222222, i: true },
                { x:  750, y:  764, c: 0xffffff, i: true },
                { x:  778, y:  771, c: 0xffffff, i: true },
                { x:  955, y:  772, c: 0xffb45e, i: true },
                { x: 1043, y:  792, c: 0x702c07, i: true },
            ],
            operaPoints: [
                { x: 932, y: 766, ox: 330, oy: 79, ad: 2000},
            ]
        }]
    }, {
        id: 12,
        name: '组队大厅_单人开始',
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   45, y:   33, c: 0x343434, i: true },
                { x:  177, y:   31, c: 0xfa9f58, i: true },
                { x:  301, y:   54, c: 0xffffff, i: true },
                { x:   28, y:  172, c: 0xfa8444, i: true },
                { x:  478, y:  975, c: 0x333333, i: true },
                { x: 1198, y:  977, c: 0xf7a758, i: true },
                { x: 1267, y:  993, c: 0x702c07, i: true },
            ],
            operaPoints: [
                { x: 1153, y: 966, ox: 270, oy: 77, ad: [1000, 3000] },
            ]
        }],
    }
]
