var multiColor = {};
var multiColorNames = [];
for (var i = 0; i < multiColorNames.length; i++) {
    multiColor[multiColorNames[i]] = require('../multi_colors/19201080/' + multiColorNames[i]);
}

module.exports = [
    null, // 0 放着占位,后续可能会放必执行的东西
    {
        id: 1,
        name: '战前布阵_确定',
        data: [
            {
                judgePoints: [
                    { x:   43, y:   29, c: 0x343436, i: true },
                    { x:   40, y:   54, c: 0x313131, i: true },
                    { x:   65, y:   59, c: 0xffffff, i: true },
                    { x:  180, y:   32, c: 0xfb9f5c, i: true },
                    { x: 1723, y:  870, c: 0xffa455, i: true },
                    { x: 1732, y:  925, c: 0xff9b50, i: true },
                    { x: 1789, y:  937, c: 0x712c05, i: true },
                    { x: 1780, y:  983, c: 0x6e2e08, i: true },
                    { x: 1792, y: 1022, c: 0xff9b50, i: true },
                ],
                operaPoints: [{ x: 1709, y: 861, ox: 169, oy: 167, ad: 5000 }]
            }
        ],
    },
    {
        id: 2,
        name: '退出结算_继续',
        data: [
            {   // 战斗胜利
                judgePoints: [
                    { x:   43, y:   47, c: 0x292929, i: true },
                    { x:   62, y:   23, c: 0x292929, i: true },
                    { x: 1792, y: 1049, c: 0x1a1a1a, i: true },
                    { x: 1833, y: 1037, c: 0x1a1a1a, i: true },
                    { x: 1821, y:  996, c: 0xfb8442, i: true },
                    { x: 1774, y:  987, c: 0xffffff, i: true },
                    { x: 1673, y:  994, c: 0xffffff, i: true },
                    { x: 1621, y: 1022, c: 0x535353, i: true },
                ],
                operaPoints: [{ x: 109, y: 138, ox: 1384, oy: 461, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:  133, y:  128, c: 0xfa8444, i: true },
                    { x:  163, y:  115, c: 0x000000, i: true },
                    { x:  232, y:  131, c: 0x000000, i: true },
                    { x:  269, y:  117, c: 0xc2c4c1, i: true },
                    { x: 1158, y:  234, c: 0xfb8344, i: true },
                    { x:  166, y:  443, c: 0xfa8446, i: true },
                    { x: 1502, y:  866, c: 0xff9c51, i: true }, // 继续上的一个点
                    { x: 1584, y:  868, c: 0x6c2d04, i: true }, // 继续上的一个点
                ],
                operaPoints: [{ x: 1479, y: 840, ox: 269, oy: 75, ad: 2000 }]
            }
        ]
    },
    {
        id: 3,
        name: '退出结算_再次挑战',
        data: [
            {   // 战斗胜利
                judgePoints: [
                    { x:   43, y:   47, c: 0x292929, i: true },
                    { x:   62, y:   23, c: 0x292929, i: true },
                    { x: 1792, y: 1049, c: 0x1a1a1a, i: true },
                    { x: 1833, y: 1037, c: 0x1a1a1a, i: true },
                    { x: 1821, y:  996, c: 0xfb8442, i: true },
                    { x: 1774, y:  987, c: 0xffffff, i: true },
                    { x: 1673, y:  994, c: 0xffffff, i: true },
                    { x: 1621, y: 1022, c: 0x535353, i: true },
                ],
                operaPoints: [{ x: 109, y: 138, ox: 1384, oy: 461, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:  133, y:  128, c: 0xfa8444, i: true },
                    { x:  163, y:  115, c: 0x000000, i: true },
                    { x:  232, y:  131, c: 0x000000, i: true },
                    { x:  269, y:  117, c: 0xc2c4c1, i: true },
                    { x: 1158, y:  234, c: 0xfb8344, i: true },
                    { x:  166, y:  443, c: 0xfa8446, i: true },
                    { x: 1502, y:  866, c: 0xff9c51, i: true }, // 继续上的一个点
                    { x: 1584, y:  868, c: 0x6c2d04, i: true }, // 继续上的一个点
                    { x: 1174, y:  852, c: 0x313131, i: true }, // 再次挑战上的一个点
                    { x: 1230, y:  865, c: 0xffffff, i: true }, // 再次挑战上的一个点
                ],
                operaPoints: [{ x: 1148, y: 839, ox: 269, oy: 75, ad: 2000 }]
            }
        ]
    },
    {
        id: 4,
        name: '限时副本发现',
        data: [{
            judgePoints: [
                { x:  769, y:  340, c: 0xfffffb, i: true },
                { x:  282, y:  429, c: 0xffffff, i: true },
                { x:  277, y:  514, c: 0xf07f43, i: true },
                { x:  366, y:  481, c: 0x515151, i: true },
                { x:  727, y:  678, c: 0xffffff, i: true },
                { x:  299, y:  658, c: 0x181818, i: true },
            ],
            operaPoints: [{ x: 889, y: 613, ox: 165, oy: 102, ad: 2000 }]
        }]
    },
    {
        id: 5,
        name: '组队大厅_邀请好友',
        data: [{
            secondJudgeDelay: 5000,
            judgePoints: [
                { x:   39, y:   35, c: 0x313131, i: true },
                { x:   64, y:   58, c: 0xfefefe, i: true },
                { x:  184, y:   30, c: 0xfba35b, i: true },
                { x:  193, y:   41, c: 0x7b3b15, i: true },
                { x:   27, y:  172, c: 0xfb8442, i: true },
                { x:  653, y:  976, c: 0x333333, i: true },
                { x: 1054, y:  980, c: 0xffad5d, i: true },
                { x: 1148, y:  986, c: 0x6b2b07, i: true },
                { x: 1862, y:   32, c: 0xffffff, i: true },
                { x: 1630, y:  540, c: 0xf4874c, i: true }, // 右边+号上的一个点
            ],
            operaPoints: [
                { x: 1597, y: 510, ox: 67, oy: 57, ad: 1000 },
                { x: 707, y: 284, ox: 175, oy: 72, ad: 500 },
                { x: 1246, y: 406, ox: 141, oy: 67, ad: 500 },
                { x: 1247, y: 560, ox: 140, oy: 66, ad: 500 },
                { x: 128, y: 211, ox: 246, oy: 672, ad: 500 },
            ]
        }],
    },
    {
        id: 6,
        name: '组队大厅_三人开始',
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   39, y:   35, c: 0x313131, i: true },
                { x:   64, y:   58, c: 0xfefefe, i: true },
                { x:  184, y:   30, c: 0xfba35b, i: true },
                { x:  193, y:   41, c: 0x7b3b15, i: true },
                { x:   27, y:  172, c: 0xfb8442, i: true },
                { x:  653, y:  976, c: 0x333333, i: true },
                { x: 1054, y:  980, c: 0xffad5d, i: true },
                { x: 1148, y:  986, c: 0x6b2b07, i: true },
                { x: 1862, y:   32, c: 0xffffff, i: true },
                { x:  724, y:  540, c: 0xf48848, i: false }, // 左边+号上的一个点
                { x: 1630, y:  540, c: 0xf4874c, i: false }, // 右边+号上的一个点
            ],
            operaPoints: [
                { x: 1036, y: 966, ox: 267, oy: 75, ad: [1000, 3000] },
            ]
        }],
    },
    {
        id: 7,
        name: '组队大厅_开始',
        data: function () {
            let clickConfig = {
                id: 7,
                name: '组队大厅_开始',
                data: [{
                    secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
                    judgePoints: [
                        { x:   39, y:   35, c: 0x313131, i: true },
                        { x:   64, y:   58, c: 0xfefefe, i: true },
                        { x:  184, y:   30, c: 0xfba35b, i: true },
                        { x:  193, y:   41, c: 0x7b3b15, i: true },
                        { x:   27, y:  172, c: 0xfb8442, i: true },
                        { x:  653, y:  976, c: 0x333333, i: true },
                        { x: 1054, y:  980, c: 0xffad5d, i: true },
                        { x: 1148, y:  986, c: 0x6b2b07, i: true },
                        { x: 1862, y:   32, c: 0xffffff, i: true },
                        { x:  724, y:  540, c: 0xf48848, i: false }, // 左边+号上的一个点
                        // { x: 1630, y:  540, c: 0xf4874c, i: false }, // 右边+号上的一个点
                    ],
                    operaPoints: [
                        { x: 1036, y: 966, ox: 267, oy: 75, ad: [1000, 3000] },
                    ]
                }],
            };
            let successTimes = 0;
            let success = false;
            while (this.commonClick(clickConfig)) {
                success = true;
                if (++successTimes >= this.userConfigs.zddt_ks_times_to_leave) {
                    this.automator.press(random(24, 129), random(24, 89), random(10, 100));
                    toastLog('发现卡车行为，退出房间。');
                    break;
                };
                this.captureScreen();
            }
            return success;
        }
    },
    {
        id: 8,
        name: '战斗中_退出战斗',
        data: function () {
            return this.commonClick({
                id: 8,
                name: '战斗中_退出战斗',
                data: [{ // 究极组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:  572, y:   47, c: 0x9e3434, i: true },
                        { x:  630, y:   48, c: 0x3392fe, i: true },
                        { x:  538, y:  170, c: 0xff7635, i: true },
                        { x:  147, y:   96, c: 0x1f87e8, i: true },
                        { x:  137, y:   84, c: 0xfefefe, i: true },
                    ],
                    operaPoints: [
                        { x: 1853, y: 37, ox: 34, oy: 32, ad: 1000 },
                        { x: 502,  y: 891, ox: 343, oy: 106, ad: 1000 },
                        { x: 1023, y: 687, ox: 325, oy: 91, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }, { // 章鱼组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:  153, y:   96, c: 0x1d88e6, i: true },
                        { x:  137, y:   83, c: 0xfffeff, i: true },
                        { x:  535, y:  170, c: 0xfd7938, i: true },
                        { x:  960, y:   43, c: 0xfdfdfd, i: true },
                        { x:  959, y:   55, c: 0xffffff, i: true },
                        { x:   32, y:   80, c: 0xfffffb, i: true },
                    ],
                    operaPoints: [
                        { x: 1853, y: 37, ox: 34, oy: 32, ad: 1000 },
                        { x: 502,  y: 891, ox: 343, oy: 106, ad: 1000 },
                        { x: 1023, y: 687, ox: 325, oy: 91, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  778, y:  209, c: 0xffffff, i: true },
                        { x:  704, y:  334, c: 0xffffff, i: true },
                        { x:  282, y:  397, c: 0xffffff, i: true },
                        { x:  268, y:  493, c: 0xffffff, i: true },
                        { x:  825, y:  499, c: 0xffffff, i: true },
                        { x:  623, y:  904, c: 0x313131, i: true },
                        { x: 1149, y:  908, c: 0xffa053, i: true },
                        { x:  605, y:  931, c: 0xd5d5d5, i: true },
                        { x: 1203, y:  939, c: 0x702f09, i: true },
                    ],
                    operaPoints: [
                        { x: 502,  y: 891, ox: 343, oy: 106, ad: 1000 },
                        { x: 1023, y: 687, ox: 325, oy: 91, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  606, y:  346, c: 0xffffff, i: true },
                        { x:  703, y:  340, c: 0xffffff, i: true },
                        { x:  852, y:  341, c: 0xffffff, i: true },
                        { x:  910, y:  347, c: 0xffffff, i: true },
                        { x:  751, y:  437, c: 0xffffff, i: true },
                        { x:  747, y:  418, c: 0x000000, i: true },
                        { x:  581, y:  699, c: 0x313131, i: true },
                        { x: 1040, y:  698, c: 0xffa254, i: true },
                        { x: 1082, y:  754, c: 0x702601, i: true },
                        { x:  625, y:  739, c: 0xfdfdfd, i: true },
                    ],
                    operaPoints: [
                        { x: 1023, y: 687, ox: 325, oy: 91, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }, {
                    judgePoints: [
                        { x:  629, y:  665, c: 0xfb8346, i: true },
                        { x:  673, y:  633, c: 0xffffff, i: true },
                        { x:  669, y:  655, c: 0x000000, i: true },
                        { x:  951, y:  538, c: 0xff911d, i: true },
                        { x: 1047, y:  539, c: 0xfa8444, i: true },
                        { x:  639, y:  425, c: 0xbdbdbd, i: true },
                    ],
                    operaPoints: [{ x: 109, y: 138, ox: 1384, oy: 461, ad: 1000 }]
                }]
            });
        },
    }, {
        id: 9,
        name: '接受邀请',
        data: [{
            judgePoints: [
                { x: 1201, y:  343, c: 0xfa8444, i: true },
                { x: 1346, y:  255, c: 0xffffff, i: true },
                { x: 1414, y:  262, c: 0xffffff, i: true },
                { x: 1615, y:  267, c: 0x000000, i: true },
                { x: 1712, y:  280, c: 0x010101, i: true },
                { x: 1714, y:  263, c: 0xffffff, i: true },
                { x: 1827, y:  179, c: 0x010007, i: true },
                { x: 1844, y:  207, c: 0x000004, i: true },
                { x: 1871, y:  192, c: 0x57f0fe, i: true },
                { x: 1882, y:  138, c: 0xf90101, i: true },
            ],
            operaPoints: [
                { x: 1697, y: 247, ox: 39, oy: 39, ad: 500 },
            ]
        }, {
            judgePoints: [
                { x: 1826, y:  173, c: 0x000100, i: true },
                { x: 1827, y:  160, c: 0x000002, i: true },
                { x: 1850, y:  170, c: 0x010200, i: true },
                { x: 1851, y:  181, c: 0x58edff, i: true },
                { x: 1878, y:  147, c: 0xfb0200, i: true },
                { x: 1899, y:  144, c: 0xfd0100, i: true },
                { x: 1871, y:  193, c: 0x57effd, i: true },
            ],
            operaPoints: [
                { x: 1825, y: 158, ox: 59, oy: 55, ad: 500 },
            ]
        }],
    }, {
        id: 10,
        name: '组队大厅_准备',
        data: [{
            judgePoints: [
                { x:   38, y:   42, c: 0x313131, i: true },
                { x:   65, y:   57, c: 0xffffff, i: true },
                { x:  184, y:   25, c: 0xfba35b, i: true },
                { x:  191, y:   43, c: 0x7a3c15, i: true },
                { x:   26, y:  169, c: 0xfa8446, i: true },
                { x: 1836, y:   53, c: 0xffffff, i: true },
                { x:  481, y:  981, c: 0x313131, i: true },
                { x: 1061, y:  996, c: 0xfb9f52, i: true },
                { x: 1149, y:  985, c: 0x6e2e08, i: true },
            ],
            operaPoints: [
                { x: 1034, y: 967, ox: 266, oy: 72, ad: [1000, 3000] },
            ]
        }],
    }, {
        id: 11,
        name: '究极_创建组队',
        data: [{
            judgePoints: [
                { x:   39, y:   28, c: 0x333333, i: true },
                { x:   63, y:   57, c: 0xffffff, i: true },
                { x:  173, y:   22, c: 0xfa9f56, i: true },
                { x:  190, y:   45, c: 0x763811, i: true },
                { x: 1156, y:  957, c: 0xffae5d, i: true },
                { x: 1259, y:  953, c: 0xffb560, i: true },
                { x: 1250, y:  978, c: 0x702f09, i: true },
                { x: 1850, y:   54, c: 0xffffff, i: true },
            ],
            operaPoints: [
                { x: 1142, y: 946, ox: 301, oy: 91, ad: 200 },
                { x: 814, y: 766, ox: 269, oy: 76, ad: 2000 },
            ]
        }, {
            judgePoints: [
                { x:  582, y:  214, c: 0xfa853f, i: true },
                { x:  597, y:  204, c: 0x161616, i: true },
                { x:  608, y:  207, c: 0xffffff, i: true },
                { x:  765, y:  797, c: 0xffffff, i: true },
                { x:  849, y:  790, c: 0xfaa052, i: true },
                { x:  923, y:  792, c: 0x732e05, i: true },
            ],
            operaPoints: [
                { x: 814, y: 766, ox: 269, oy: 76, ad: 2000 },
            ]
        }]
    }, {
        id: 12,
        name: '组队大厅_单人开始',
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   47, y:   31, c: 0x343434, i: true },
                { x:  180, y:   33, c: 0xfba059, i: true },
                { x:   27, y:  171, c: 0xfc8543, i: true },
                { x:  303, y:   51, c: 0xffffff, i: true },
                { x:  559, y:  979, c: 0x353535, i: true },
                { x: 1074, y:  991, c: 0xf9a253, i: true },
                { x: 1150, y:  993, c: 0x6c2d04, i: true },
            ],
            operaPoints: [
                { x: 1036, y: 966, ox: 267, oy: 75, ad: [1000, 3000] },
            ]
        }],
    }
]
