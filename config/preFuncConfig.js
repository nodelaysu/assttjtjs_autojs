module.exports = [
    {
        name: '自定义',
        funcNumbers: []
    },
    {
        name: '通用探索循环',
        funcNumbers: [1, 3, 4]
    },
    {
        name: '通用进入退出',
        funcNumbers: [1, 2, 4]
    },
    {
        name: '究极发车_双人',
        funcNumbers: [7, 5, 8, 11]
    },
    {
        name: '究极发车_三人',
        funcNumbers: [6, 5, 8, 11]
    },
    {
        name: '究极组队_躺',
        funcNumbers: [2, 9, 10]
    },
    {
        name: '究极组队_AI打手',
        funcNumbers: [9, 10, 8]
    },
    {
        name: '章鱼_刷家族活跃',
        funcNumbers: [12, 8]
    }
];