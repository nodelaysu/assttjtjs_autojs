var multiColor = {};
var multiColorNames = [];
for (var i = 0; i < multiColorNames.length; i++) {
    multiColor[multiColorNames[i]] = require('../multi_colors/1280720/' + multiColorNames[i]);
}

module.exports = [
    null, // 0 放着占位,后续可能会放必执行的东西
    {
        id: 1,
        name: '战前布阵_确定',
        data: [
            {
                judgePoints: [
                    { x:   20, y:   33, c: 0x2f2f2f, i: true },
                    { x:   44, y:   37, c: 0xffffff, i: true },
                    { x:  119, y:   22, c: 0xfaa058, i: true },
                    { x: 1242, y:   35, c: 0xffffff, i: true },
                    { x: 1248, y:   26, c: 0x383838, i: true },
                    { x: 1160, y:  591, c: 0xff9e52, i: true },
                    { x: 1186, y:  656, c: 0x702c07, i: true },
                ],
                operaPoints: [{ x: 1137, y: 575, ox: 115, oy: 112, ad: 5000 }]
            }
        ],
    },
    {
        id: 2,
        name: '退出结算_继续',
        data: [
            { // 战斗胜利
                judgePoints: [
                    { x:   26, y:   33, c: 0x292929, i: true },
                    { x: 1064, y:  659, c: 0xffffff, i: true },
                    { x: 1087, y:  661, c: 0xffffff, i: true },
                    { x: 1161, y:  659, c: 0xffffff, i: true },
                    { x: 1160, y:  684, c: 0x1a1a1a, i: true },
                    { x: 1214, y:  664, c: 0xfa8444, i: true },
                    { x: 1201, y:  664, c: 0x1a1a1a, i: true },
                ],
                operaPoints: [{ x: 57, y: 55, ox: 1098, oy: 62, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:   89, y:   90, c: 0xfa8444, i: true },
                    { x:  106, y:   86, c: 0x000000, i: true },
                    { x:  168, y:   83, c: 0x000000, i: true },
                    { x:  192, y:   83, c: 0x000000, i: true },
                    { x:  278, y:   94, c: 0xc2c4c2, i: true },
                    { x:  179, y:   79, c: 0xc2c4c2, i: true },
                    { x:  771, y:  159, c: 0xfa8444, i: true },
                    // { x:  782, y:  580, c: 0x2b2b2b, i: true }, // 再次挑战上面的一个点
                    { x: 1019, y:  584, c: 0xff9a50, i: true }, // 继续上面的一个点
                ],
                operaPoints: [{ x: 985, y: 558, ox: 180, oy: 51, ad: 2000 }]
            }
        ]
    },
    {
        id: 3,
        name: '退出结算_再次挑战',
        data: [
            { // 战斗胜利
                judgePoints: [
                    { x:   26, y:   33, c: 0x292929, i: true },
                    { x: 1064, y:  659, c: 0xffffff, i: true },
                    { x: 1087, y:  661, c: 0xffffff, i: true },
                    { x: 1161, y:  659, c: 0xffffff, i: true },
                    { x: 1160, y:  684, c: 0x1a1a1a, i: true },
                    { x: 1214, y:  664, c: 0xfa8444, i: true },
                    { x: 1201, y:  664, c: 0x1a1a1a, i: true },
                ],
                operaPoints: [{ x: 57, y: 55, ox: 1098, oy: 62, ad: 1000 }]
            }, { // 战斗结算
                judgePoints: [
                    { x:   89, y:   90, c: 0xfa8444, i: true },
                    { x:  106, y:   86, c: 0x000000, i: true },
                    { x:  168, y:   83, c: 0x000000, i: true },
                    { x:  192, y:   83, c: 0x000000, i: true },
                    { x:  278, y:   94, c: 0xc2c4c2, i: true },
                    { x:  179, y:   79, c: 0xc2c4c2, i: true },
                    { x:  771, y:  159, c: 0xfa8444, i: true },
                    { x:  782, y:  580, c: 0x2b2b2b, i: true }, // 再次挑战上面的一个点
                    { x: 1019, y:  584, c: 0xff9a50, i: true }, // 继续上面的一个点
                ],
                operaPoints: [{ x: 766, y: 557, ox: 179, oy: 50, ad: 2000 }]
            }
        ]
    },
    {
        id: 4,
        name: '限时副本发现',
        data: [{
            judgePoints: [
                { x:  513, y:  222, c: 0xffffff, i: true },
                { x:  523, y:  214, c: 0xffffff, i: true },
                { x:  164, y:  332, c: 0xef7f42, i: true },
                { x:  233, y:  320, c: 0x515151, i: true },
                { x:  528, y:  308, c: 0xfcaf85, i: true },
                { x:  442, y:  449, c: 0xffffff, i: true },
            ],
            operaPoints: [{ x: 599, y: 408, ox: 242, oy: 67, ad: 2000 }]
        }]
    },
    {
        id: 5,
        name: '组队大厅_邀请好友',
        data: [{
            secondJudgeDelay: 5000,
            judgePoints: [
                { x:   29, y:   28, c: 0x323232, i: true },
                { x:   46, y:   39, c: 0xffffff, i: true },
                { x:  121, y:   23, c: 0xfba059, i: true },
                { x: 1233, y:   35, c: 0xffffff, i: true },
                { x:  359, y:  655, c: 0x333333, i: true },
                { x:  373, y:  666, c: 0xffffff, i: true },
                { x:  734, y:  665, c: 0xfea254, i: true },
                { x:  773, y:  661, c: 0x702c07, i: true },
                { x:   18, y:  114, c: 0xfa8444, i: true },
                // { x:  474, y:  359, c: 0xfa8444, i: true }, // 左边的 “+” 上的一个点
                { x: 1076, y:  359, c: 0xfa8444, i: true }, // 右边的 “+” 上的一个点
            ],
            operaPoints: [
                { x: 1067, y: 341, ox: 42, oy: 42, ad: 1000 },
                { x: 471, y: 190, ox: 118, oy: 48, ad: 500 },
                { x: 830, y: 268, ox: 94, oy: 48, ad: 500 },
                { x: 830, y: 372, ox: 94, oy: 46, ad: 500 },
                // { x: 110, y: 136, ox: 188, oy: 453, ad: [10000, 20000] },
                { x: 110, y: 136, ox: 188, oy: 453, ad: 500 },
            ]
        }],
    },
    {
        id: 6,
        name: '组队大厅_三人开始',
        
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   29, y:   28, c: 0x323232, i: true },
                { x:   46, y:   39, c: 0xffffff, i: true },
                { x:  121, y:   23, c: 0xfba059, i: true },
                { x: 1233, y:   35, c: 0xffffff, i: true },
                { x:  359, y:  655, c: 0x333333, i: true },
                { x:  373, y:  666, c: 0xffffff, i: true },
                { x:  734, y:  665, c: 0xfea254, i: true },
                { x:  773, y:  661, c: 0x702c07, i: true },
                { x:   18, y:  114, c: 0xfa8444, i: true },
                { x:  474, y:  359, c: 0xfa8444, i: false }, // 左边的 “+” 上的一个点
                { x: 1076, y:  359, c: 0xfa8444, i: false }, // 右边的 “+” 上的一个点
            ],
            operaPoints: [
                { x: 691, y: 647, ox: 175, oy: 45, ad: 1000 },
            ]
        }],
    },
    {
        id: 7,
        name: '组队大厅_开始',
        data: function () {
            let clickConfig = {
                name: '组队大厅_开始',
                data: [{
                    secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
                    judgePoints: [
                        { x:   29, y:   28, c: 0x323232, i: true },
                        { x:   46, y:   39, c: 0xffffff, i: true },
                        { x:  121, y:   23, c: 0xfba059, i: true },
                        { x: 1233, y:   35, c: 0xffffff, i: true },
                        { x:  359, y:  655, c: 0x333333, i: true },
                        { x:  373, y:  666, c: 0xffffff, i: true },
                        { x:  734, y:  665, c: 0xfea254, i: true },
                        { x:  773, y:  661, c: 0x702c07, i: true },
                        { x:   18, y:  114, c: 0xfa8444, i: true },
                        { x:  474, y:  359, c: 0xfa8444, i: false }, // 左边的 “+” 上的一个点
                        // { x: 1076, y:  359, c: 0xfa8444, i: false }, // 右边的 “+” 上的一个点
                    ],
                    operaPoints: [
                        { x: 691, y: 647, ox: 175, oy: 45, ad: 1000 },
                    ]
                }],
            };
            let successTimes = 0;
            let success = false;
            while (this.commonClick(clickConfig)) {
                success = true;
                if (++successTimes >= this.userConfigs.zddt_ks_times_to_leave) {
                    this.automator.press(random(15, 87), random(14, 60), random(10, 100));
                    toastLog('发现卡车行为，退出房间。');
                    break;
                };
                this.captureScreen();
            }
            return success;
        }
    },
    {
        id: 8,
        name: '战斗中_退出战斗',
        data: function () {
            return this.commonClick({
                id: 8,
                name: '战斗中_退出战斗',
                data: [{ // 究极组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:  389, y:   35, c: 0x7c2b2d, i: true },
                        { x:  425, y:   31, c: 0x3492ff, i: true },
                        // { x:  114, y:   53, c: 0xe66119, i: true },
                        { x:  114, y:   62, c: 0x228aea, i: true },
                        { x:   91, y:   54, c: 0xffffff, i: true },
                        { x:  367, y:  113, c: 0xff7835, i: true },
                    ],
                    operaPoints: [
                        { x: 1235, y: 24, ox: 23, oy: 23, ad: 1000 },
                        { x: 336, y: 593, ox: 224, oy: 68, ad: 1000 },
                        { x: 680, y: 456, ox: 218, oy: 66, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }, { // 章鱼组队
                    secondJudgeDelay: this.userConfigs.zdz_tczd_secondJudgeDelay,
                    judgePoints: [
                        { x:   21, y:   55, c: 0xffffff, i: true },
                        { x:   91, y:   55, c: 0xffffff, i: true },
                        { x:  101, y:   64, c: 0x1e87e8, i: true },
                        { x:  639, y:   28, c: 0xffffff, i: true },
                        { x:  639, y:   36, c: 0xffffff, i: true },
                        { x:  358, y:  113, c: 0xff7835, i: true },
                    ],
                    operaPoints: [
                        { x: 1235, y: 24, ox: 23, oy: 23, ad: 1000 },
                        { x: 336, y: 593, ox: 224, oy: 68, ad: 1000 },
                        { x: 680, y: 456, ox: 218, oy: 66, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  519, y:  139, c: 0xffffff, i: true },
                        { x:  187, y:  244, c: 0xffffff, i: true },
                        { x:  183, y:  315, c: 0xffffff, i: true },
                        { x:  588, y:  333, c: 0xffffff, i: true },
                        { x:  362, y:  615, c: 0x2e2e2e, i: true },
                        { x:  773, y:  603, c: 0xffa154, i: true },
                        { x:  426, y:  622, c: 0xdadada, i: true },
                        { x:  794, y:  620, c: 0x702c07, i: true },
                    ],
                    operaPoints: [
                        { x: 336, y: 593, ox: 224, oy: 68, ad: 1000 },
                        { x: 680, y: 456, ox: 218, oy: 66, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                },{
                    judgePoints: [
                        { x:  461, y:  280, c: 0x000000, i: true },
                        { x:  477, y:  250, c: 0xffffff, i: true },
                        { x:  510, y:  318, c: 0xffffff, i: true },
                        { x:  500, y:  291, c: 0xffffff, i: true },
                        { x:  522, y:  283, c: 0x000000, i: true },
                        { x:  593, y:  295, c: 0x000000, i: true },
                        { x:  639, y:  328, c: 0xffffff, i: true },
                        { x:  450, y:  466, c: 0x313131, i: true },
                        { x:  693, y:  467, c: 0xffa053, i: true },
                        { x:  416, y:  492, c: 0xffffff, i: true },
                        { x:  472, y:  493, c: 0xffffff, i: true },
                        { x:  779, y:  491, c: 0x702c07, i: true },
                    ],
                    operaPoints: [
                        { x: 680, y: 456, ox: 218, oy: 66, ad: this.userConfigs.zdz_tczd_AfterTime },
                    ]
                }, { // 失败
                    judgePoints: [
                        { x:  418, y:  432, c: 0xfa8444, i: true },
                        { x:  457, y:  429, c: 0xffffff, i: true },
                        { x:  464, y:  443, c: 0x000000, i: true },
                        { x:  426, y:  264, c: 0xb3b3b3, i: true },
                        { x:  637, y:  358, c: 0xff921e, i: true },
                    ],
                    operaPoints: [{ x: 57, y: 55, ox: 1098, oy: 62, ad: this.userConfigs.zdz_tczd_AfterTime }]
                }]
            });
        },
    }, {
        id: 9,
        name: '接受邀请',
        data: [{
            judgePoints: [
                { x:  803, y:  228, c: 0xfa8444, i: true },
                { x:  994, y:  166, c: 0xffffff, i: true },
                { x: 1076, y:  178, c: 0x000000, i: true },
                { x: 1218, y:  120, c: 0x000000, i: true },
                { x: 1247, y:  127, c: 0x55f1ff, i: true },
                { x: 1251, y:   94, c: 0xff0000, i: true },
                { x:  805, y:  160, c: 0xffffff, i: true },
                { x: 1142, y:  186, c: 0x000000, i: true },
            ],
            operaPoints: [
                { x: 1134, y: 166, ox: 23, oy: 21, ad: 500 },
            ]
        }, {
            judgePoints: [
                { x: 1217, y:  113, c: 0x000000, i: true },
                { x: 1232, y:  121, c: 0x55f1ff, i: true },
                { x: 1248, y:  116, c: 0x000000, i: true },
                { x: 1247, y:  128, c: 0x55f1ff, i: true },
                { x: 1234, y:  138, c: 0x000000, i: true },
                { x: 1243, y:  129, c: 0x000203, i: true },
                { x: 1251, y:  101, c: 0xff0000, i: true },
                { x: 1266, y:  100, c: 0xff0000, i: true },
            ],
            operaPoints: [
                { x: 1216, y: 106, ox: 40, oy: 33, ad: 500 },
            ]
        }],
    }, {
        id: 10,
        name: '组队大厅_准备',
        data: [{
            judgePoints: [
                { x:   26, y:   25, c: 0x313131, i: true },
                { x:  122, y:   20, c: 0xfba25a, i: true },
                { x: 1225, y:   34, c: 0xffffff, i: true },
                { x: 1240, y:   28, c: 0x343434, i: true },
                { x:   17, y:  106, c: 0xfa8444, i: true },
                { x:  357, y:  652, c: 0x343434, i: true },
                { x:  702, y:  656, c: 0xf6a054, i: true },
                { x:  766, y:  655, c: 0x702c07, i: true },
            ],
            operaPoints: [
                { x: 691, y: 646, ox: 177, oy: 46, ad: [1000, 3000] },
            ]
        }],
    }, {
        id: 11,
        name: '究极_创建组队',
        data: [{
            judgePoints: [
                { x:   27, y:   26, c: 0x313131, i: true },
                { x:   44, y:   37, c: 0xffffff, i: true },
                { x:  184, y:   35, c: 0x19292c, i: true },
                { x:  201, y:   33, c: 0xffffff, i: true },
                { x:  776, y:  636, c: 0xfeb15c, i: true },
                { x:  833, y:  652, c: 0x702c07, i: true },
                { x:  904, y:  641, c: 0xfcaf5b, i: true },
                { x: 1233, y:   37, c: 0xffffff, i: true },
            ],
            operaPoints: [
                { x: 761, y: 630, ox: 198, oy: 60, ad: 200 },
                { x: 543, y: 510, ox: 179, oy: 50, ad: 2000 },
            ]
        }, {
            judgePoints: [
                { x:  385, y:  142, c: 0xfa8444, i: true },
                { x:  401, y:  133, c: 0x191919, i: true },
                { x:  408, y:  145, c: 0xffffff, i: true },
                { x:  447, y:  517, c: 0xffffff, i: true },
                { x:  563, y:  527, c: 0xf9a053, i: true },
                { x:  612, y:  531, c: 0x702c07, i: true },
            ],
            operaPoints: [
                { x: 543, y: 510, ox: 179, oy: 50, ad: 2000 },
            ]
        }]
    }, {
        id: 12,
        name: '组队大厅_单人开始',
        data: [{
            secondJudgeDelay: 2000, // 新增参数,如果judgePoints通过，等待这个时间再进行检测，通过的话再执行operaPoints
            judgePoints: [
                { x:   22, y:   22, c: 0x313131, i: true },
                { x:  121, y:   17, c: 0xfba25a, i: true },
                { x:  217, y:   38, c: 0xffffff, i: true },
                { x:  356, y:  651, c: 0x333333, i: true },
                { x:  707, y:  652, c: 0xf9a758, i: true },
                { x:  764, y:  662, c: 0x702c07, i: true },
            ],
            operaPoints: [
                { x: 691, y: 647, ox: 175, oy: 45, ad: 1000 },
            ]
        }],
    }
]
