findMultiColorInRegionFuzzy( 0xc6ae9d, "304|7|0xccbba3,112|82|0x827d77,152|82|0x807b74,192|82|0x827d76,231|82|0xb3a28b,271|82|0xb3a28b", 90, 0, 0, 1279, 719)

function findMultiColorInRegionFuzzy(firstColor, colors, a, b, c, d, e) {
    var ret = {
        firstColor: firstColor,
        colors: []
    };
    var colors = colors.split(',');
    for (let i = 0; i < colors.length; i++) {
        colors[i] = colors[i].split('|');
        for (let j = 0; j < colors[i].length; j++) {
            colors[i][j] = parseInt(colors[i][j]);
        }
    }
    ret.colors = colors;
    console.log(JSON.stringify(ret));
    return ret;
}